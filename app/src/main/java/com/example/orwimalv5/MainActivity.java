package com.example.orwimalv5;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private static final String MANDJUKIC_INSPIRATION="Mario Madžukić won the first silver medal at the World Cup in Croatia!";
    private static final String PERKOVIC_INSPIRATION ="Sandra Perković won the fifth European gold in 2018!";
    private static final String DUVNJAK_INSPIRATION ="Domagoj Duvnjak won seven medals!";
    Button btnInspiration;
    RadioGroup rgPerson;
    RadioButton rbtnPerson1, rbtnPerson2, rbtnPerson3;
    Button btnDescription;
    EditText etDescription;
    TextView tvDescription1, tvDescription2, tvDescription3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUI();
    }

    private void initializeUI(){
        this.btnInspiration = (Button) findViewById(R.id.btnInspiration);
        this.btnInspiration.setOnClickListener(this);
        rgPerson =(RadioGroup) findViewById(R.id.rgPerson);
        rbtnPerson1 =(RadioButton) findViewById(R.id.rbtnPerson1);
        rbtnPerson2 =(RadioButton) findViewById(R.id.rbtnPerson2);
        rbtnPerson3 =(RadioButton) findViewById(R.id.rbtnPerson3);
        tvDescription1 =(TextView) findViewById(R.id.tvDescription1);
        tvDescription2 =(TextView) findViewById(R.id.tvDescription2);
        tvDescription3 =(TextView) findViewById(R.id.tvDescription3);
        etDescription = (EditText) findViewById(R.id.etDescription);
        btnDescription =(Button) findViewById(R.id.btnDescription);
        btnDescription.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                if(rbtnPerson1.isChecked()){
                    tvDescription1.setText("Mario Mandžukić \n1986.-... \n"+etDescription.getText().toString());
                }
                else if(rbtnPerson2.isChecked()){
                    tvDescription2.setText("Sandra Perković \n1990.-... \n"+etDescription.getText().toString());
                }
                else if(rbtnPerson3.isChecked()){
                    tvDescription3.setText("Domagoj Duvnjak \n1988.-... \n"+etDescription.getText().toString());
                }
            }
        });
    }

    public void onClick(View view){
        int n=3;
        final int random = new Random().nextInt(n);
        if(random==0)
            this.displayToast(MANDJUKIC_INSPIRATION);
        else if(random==1)
            this.displayToast(PERKOVIC_INSPIRATION);
        else if(random==2)
            this.displayToast(DUVNJAK_INSPIRATION);
    }

    private void displayToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

}